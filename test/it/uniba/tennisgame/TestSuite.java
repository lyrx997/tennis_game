package it.uniba.tennisgame;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.*;

@RunWith(Suite.class)
@SuiteClasses( {PlayerTest.class, GameTest.class} )

public class TestSuite {}