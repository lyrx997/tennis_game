package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {
	
	@Test
	public void ScoreShouldBeIncreased() {
		
		//Arrange
		Player p = new Player("Federer", 0);
		
		//Act
		p.incrementScore();
		
		//Assert
		assertEquals(1, p.getScore());
		
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		
		//Arrange
		Player p = new Player("Federer", 0);
		
		//Act
		
		//Assert
		assertEquals(0, p.getScore());
		
	}
	
	@Test
	public void scoreShouldBeLove() {
		
		//Arrange
		Player p = new Player("Federer", 0);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("love", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeFifteen() {
		
		//Arrange
		Player p = new Player("Federer", 1);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("fifteen", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeThirty() {
		
		//Arrange
		Player p = new Player("Federer", 2);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("thirty", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeForty() {
		
		//Arrange
		Player p = new Player("Federer", 3);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertEquals("forty", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeNullIfNegative() {
		
		//Arrange
		Player p = new Player("Federer", -1);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void scoreShouldBeNullIfFourOrMore() {
		
		//Arrange
		Player p = new Player("Federer", 4);
		
		//Act
		String scoreAsString = p.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void ShouldBeTie() {
		
		//Arrange
		Player p1 = new Player("Federer", 1);
		Player p2 = new Player("Nadal", 1);
		
		//Act
		boolean result = p1.isTieWith(p2);
		
		//Assert
		assertTrue(result);
		
	}
	
	@Test
	public void ShouldNotBeTie() {
		
		//Arrange
		Player p1 = new Player("Federer", 1);
		Player p2 = new Player("Nadal", 2);
		
		//Act
		boolean result = p1.isTieWith(p2);
		
		//Assert
		assertFalse(result);
		
	}
	
	@Test
	public void PlayerShouldHaveAtLeastFortyPoints() {
		
		//Arrange
		Player p = new Player("Federer", 3);
		
		//Act
		boolean result = p.hasAtLeastFortyPoints();
		
		//Assert
		assertTrue(result);
		
	}
	
	@Test
	public void PlayerShouldNotHaveAtLeastFortyPoints() {
		
		//Arrange
		Player p = new Player("Federer", 2);
		
		//Act
		boolean result = p.hasAtLeastFortyPoints();
		
		//Assert
		assertFalse(result);
		
	}
	
	@Test
	public void PlayerShouldHaveLessThanFortyPoints() {
		
		//Arrange
		Player p = new Player("Federer", 2);
		
		//Act
		boolean result = p.hasLessThanFortyPoints();
		
		//Assert
		assertTrue(result);
		
	}
	
	@Test
	public void PlayerShouldNotHaveLessThanFortyPoints() {
		
		//Arrange
		Player p = new Player("Federer", 4);
		
		//Act
		boolean result = p.hasLessThanFortyPoints();
		
		//Assert
		assertFalse(result);
		
	}
	
	@Test
	public void PlayerShouldHaveMoreThanFortyPoints() {
		
		//Arrange
		Player p = new Player("Federer", 4);
		
		//Act
		boolean result = p.hasMoreThanFourtyPoints();
		
		//Assert
		assertTrue(result);
		
	}
	
	@Test
	public void PlayerShouldNotHaveMoreThanFortyPoints() {
		
		//Arrange
		Player p = new Player("Federer", 3);
		
		//Act
		boolean result = p.hasMoreThanFourtyPoints();
		
		//Assert
		assertFalse(result);
		
	}
	
	@Test
	public void shouldHaveOnePointAdvantageOn() {
		
		//Arrange
		Player p1 = new Player("Federer", 4);
		Player p2 = new Player("Nadal", 3);
		
		//Act
		boolean result = p1.hasOnePointAdvantageOn(p2);
		
		//Assert
		assertTrue(result);
		
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantageOn() {
		
		//Arrange
		Player p1 = new Player("Federer", 3);
		Player p2 = new Player("Nadal", 3);
		
		//Act
		boolean result = p1.hasOnePointAdvantageOn(p2);
		
		//Assert
		assertFalse(result);
		
	}
	
	@Test
	public void shouldHaveAtLeastTwoPointsAdvantageOn() {
		
		//Arrange
		Player p1 = new Player("Federer", 4);
		Player p2 = new Player("Nadal", 2);
		
		//Act
		boolean result = p1.hasAtLeastTwoPointsAdvantageOn(p2);
		
		//Assert
		assertTrue(result);
		
	}
	
	@Test
	public void shouldNotHaveAtLeastTwoPointsAdvantageOn() {
		
		//Arrange
		Player p1 = new Player("Federer", 3);
		Player p2 = new Player("Nadal", 3);
		
		//Act
		boolean result = p1.hasAtLeastTwoPointsAdvantageOn(p2);
		
		//Assert
		assertFalse(result);
		
	}
	
	

}
