package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testFifteenThirty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Federer fifteen - Nadal thirty", status);
	}
	
	@Test
	public void testFortyThirty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Federer forty - Nadal thirty", status);
	}
	
	@Test
	public void testPlayerOneWins() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		
		//Necessario incrementare gradualmente il punteggio del primo
		//giocatore perche' altrimenti, con un punteggio 4-0,
		//Federer avrebbe gia' vinto.
		
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Federer wins", status);
	}
	
	@Test
	public void testFifteenForty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Federer fifteen - Nadal forty", status);
	}
	
	@Test
	public void testDeuce() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Deuce", status);
	}
	
	@Test
	public void testAdvantagePlayerOne() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		g.incrementPlayerScore( g.getPlayerName1() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Advantage Federer", status);
	}
	
	@Test
	public void testPlayerTwoWins() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		
		//Necessario incrementare gradualmente il punteggio del secondo
		//giocatore perche' altrimenti, con un punteggio 4-0,
		//Nadal avrebbe gia' vinto.
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Nadal wins", status);
	}
	
	@Test
	public void testAdvantagePlayerTwo() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Advantage Nadal", status);
	}
	
	@Test
	public void testDeuceAfterAdvantage() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		
		//Arrange
		Game g = new Game("Federer", "Nadal");
		
		//Act
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName1() );
		
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		g.incrementPlayerScore( g.getPlayerName1() );
		g.incrementPlayerScore( g.getPlayerName2() );
		
		String status = g.getGameStatus();
		
		//Assert
		assertEquals("Deuce", status);
	}

}
